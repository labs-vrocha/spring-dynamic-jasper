# spring-dynamic-jasper

Laboratório para verificar o funcionamento da lib dynamic-jasper no java/springboot


## Versões utilizadas

- Java 8
- Docker 20.10.12, build e91ed57
- Maven 3.6.3
- docker-compose 1.29.2, build 5becea4c

## Como executar este projeto

- Na raiz do projeto, execute: 

docker-compose up 

- Na raiz do projeto, execute: 

mvn clean spring-boot:run 

- Insira alguns registros para teste na tabela "ELEITOR". Utilize o Intellij ou o SQLDeveloper. 

- No seu navegador, acesse a url http://localhost:8080/eleitores/report

Um relatório (eleitores.pdf) será gerado na pasta ./target
