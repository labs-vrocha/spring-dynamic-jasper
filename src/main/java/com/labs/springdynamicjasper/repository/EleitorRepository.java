package com.labs.springdynamicjasper.repository;

import com.labs.springdynamicjasper.core.StandardRepository;
import com.labs.springdynamicjasper.domain.Eleitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EleitorRepository extends JpaRepository<Eleitor, Long> {

}
