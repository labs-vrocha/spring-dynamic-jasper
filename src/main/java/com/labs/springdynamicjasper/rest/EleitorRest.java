package com.labs.springdynamicjasper.rest;

import com.labs.springdynamicjasper.domain.Eleitor;
import com.labs.springdynamicjasper.repository.EleitorRepository;
import com.labs.springdynamicjasper.service.ReportService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("eleitores")
public class EleitorRest {

    @Autowired
    private EleitorRepository eleitorRepository;

    @Autowired
    private ReportService reportService;

    @GetMapping
    public List findAll(){
        return eleitorRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Eleitor> findById(@PathVariable Long id){
        Optional<Eleitor> eleitor = eleitorRepository.findById(id);
        if(eleitor.isPresent()){
            return new ResponseEntity<Eleitor>(eleitor.get(), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "/report")
    public void generateReport() throws JRException, FileNotFoundException {
        reportService.generateEleitorReport();
    }

//    public ResponseEntity<?> export() {
//        return eleitorRepository.export().convert(ep.getREsturnType);
//    }
}
