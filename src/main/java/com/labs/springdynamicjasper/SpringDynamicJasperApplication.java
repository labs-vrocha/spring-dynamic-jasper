package com.labs.springdynamicjasper;

import com.labs.springdynamicjasper.core.StandardRepositoryImpl;
import com.labs.springdynamicjasper.domain.Eleitor;
import com.labs.springdynamicjasper.repository.EleitorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.metamodel.EntityType;

@SpringBootApplication
@EnableJpaRepositories(repositoryBaseClass = StandardRepositoryImpl.class)
public class SpringDynamicJasperApplication implements CommandLineRunner {

    @Autowired
    EntityManager em;

    @Autowired
    private EleitorRepository eleitorRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpringDynamicJasperApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        if (eleitorRepository.count() == 0) {
            Eleitor eleitor = new Eleitor();
            eleitor.setCpf("000000");
            eleitor.setNaturalidade("Brasileira");
            eleitor.setNome("Fulano");
            eleitor.setNomeMae("Cicrana");
            eleitorRepository.save(eleitor);
        }


        EntityType<?> et = em.getMetamodel().getEntities().stream()
            .filter(entityType -> entityType.getName().equals("Eleitor"))
            .findFirst()
            .orElseThrow(EntityNotFoundException::new);
        et.getAttributes().forEach(attribute -> System.out.println(attribute.getName()));
    }
}
