package com.labs.springdynamicjasper.service;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.constants.*;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.Transparency;
import com.labs.springdynamicjasper.repository.EleitorRepository;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.metamodel.EntityType;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

@Service
public class ReportService{

    @Autowired
    EntityManager em;

    @Autowired
    EleitorRepository eleitorRepository;

    public DynamicReportBuilder generateReportModel(){

        Style headerStyle = new Style();

        headerStyle.setBackgroundColor(new Color(230,230,230));
        headerStyle.setBorderBottom(Border.THIN());
        headerStyle.setBorderColor(Color.black);
        headerStyle.setVerticalAlign(VerticalAlign.MIDDLE);
        headerStyle.setTransparency(Transparency.OPAQUE);

        Style titleStyle = new Style();
        titleStyle.setHorizontalAlign(HorizontalAlign.CENTER);
        titleStyle.setFont(new Font());

        Style subtitleStyleParent = new Style("subtitleParent");
        subtitleStyleParent.setBackgroundColor(Color.CYAN);
        subtitleStyleParent.setTransparency(Transparency.OPAQUE);
        Style subtitleStyle = Style.createBlankStyle("subtitleStyle","subtitleParent");

        Style amountStyle = new Style(); amountStyle.setHorizontalAlign(HorizontalAlign.RIGHT);

        DynamicReportBuilder drb = new DynamicReportBuilder();

        drb.setTitle("Tribunal Regional Eleitoral do Pará")
                .setSubtitle("Secretaria de Tecnologia da Informação").setDefaultStyles(titleStyle,subtitleStyle,headerStyle,null)
                .addStyle(subtitleStyleParent);

        drb.setUseFullPageWidth(true);

        return drb;
    }

    public void generateEntityReport(String entityName) throws FileNotFoundException, JRException {

        DynamicReportBuilder drb = generateReportModel();

        EntityType<?> et = em.getMetamodel().getEntities().stream()
                .filter(entityType -> entityType.getName().equalsIgnoreCase(entityName))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
        et.getAttributes().forEach(attribute -> drb.addColumn(ColumnBuilder.getNew()
                .setTitle(attribute.getName())
                .setColumnProperty(attribute.getName(),attribute.getJavaType())
                .build()));

        DynamicReport dr = drb.build();

        JRDataSource ds = new JRBeanCollectionDataSource(this.eleitorRepository.findAll());

        JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), ds);

        SimpleExporterInput sei = new SimpleExporterInput(jp);

        FileOutputStream fos = new FileOutputStream(new File("target/" + et.getName()+ ".pdf"));

        OutputStreamExporterOutput oseo = new SimpleOutputStreamExporterOutput(fos);

        JRPdfExporter exporter = new JRPdfExporter();

        exporter.setExporterInput(sei);

        exporter.setExporterOutput(oseo);

        exporter.exportReport();


    }

    public void generateEleitorReport() throws JRException, FileNotFoundException {

        DynamicReportBuilder drb = generateReportModel();

        EntityType<?> et = em.getMetamodel().getEntities().stream()
                .filter(entityType -> entityType.getName().equals("Eleitor"))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
        et.getAttributes().forEach(attribute -> drb.addColumn(ColumnBuilder.getNew()
                .setTitle(attribute.getName())
                .setColumnProperty(attribute.getName(),attribute.getJavaType())
                .build()));

        String entityName = "Eleitor";

        DynamicReport dr = drb.build();

        JRDataSource ds = new JRBeanCollectionDataSource(this.eleitorRepository.findAll());

        JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), ds);

        SimpleExporterInput sei = new SimpleExporterInput(jp);

//        FileOutputStream fos = new FileOutputStream(new File("target/" + et.getName()+ ".pdf"));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        OutputStreamExporterOutput oseo = new SimpleOutputStreamExporterOutput(baos);

        JRPdfExporter exporter = new JRPdfExporter();

        exporter.setExporterInput(sei);

        exporter.setExporterOutput(oseo);

        exporter.exportReport();
    }

//    public <T, ID> ByteArrayOutputStream export(JpaEntityInformation<T, ID> entityInformation, List<T> dataSource) {
//
//    }

}
