package com.labs.springdynamicjasper.core;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class StandardRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements StandardRepository<T, ID> {

    private final JpaEntityInformation<T, ?> entityInformation;

    private final EntityManager entityManager;

    public StandardRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityInformation = entityInformation;
        this.entityManager = entityManager;
    }

    @Override
    public List<T> export(Specification<T> specification, ExtraOptions options) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityInformation.getJavaType());
        Root<T> root = cq.from(this.entityInformation.getJavaType());
        applySpecification(specification, root, cq, cb);

        TypedQuery<T> q = entityManager.createQuery(cq);

        return q.getResultList();
    }

    protected <X> void applySpecification(Specification<X> specification, Root<X> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
        if (Objects.nonNull(specification) && Objects.nonNull(specification.toPredicate(root, cq, cb))) {
            cq.where(specification.toPredicate(root, cq, cb));
        }
    }
}
